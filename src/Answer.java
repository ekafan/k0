import java.util.*;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Answer {

   public static void main (String[] param) throws InterruptedException {

      // conversion double -> String

      double d = 5.6;
      String string = String.valueOf(d);
      System.out.println(string);

      // conversion String -> int

      String str = "10";
      try {
       int in = Integer.parseInt(str);
       System.out.println(in);
      } catch (NumberFormatException e) {
          System.out.println("string does not contain a parsable integer");
      }


      // "HH:mm:ss" HH - 24h notation

      DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
      LocalDateTime now = LocalDateTime.now();
      System.out.println(dtf.format(now));

      // cos 45 deg

      double x = 45.0;
      double resultCos = Math.cos(Math.toRadians(x));
      System.out.println(resultCos);

      // table of square roots

      for (double number = 0; number <= 100; number = number + 5) {
         System.out.println(number + " -> " + Math.sqrt(number));
      }

      String firstString = "ABcd12";
      String result = reverseCase (firstString);
      System.out.println ("\"" + firstString + "\" -> \"" + result + "\"");

      // reverse string "1234ab" -> "ba4321"

       String st = "1234ab";
       StringBuilder sb = new StringBuilder(st);
       System.out.println("1234ab -> " + sb.reverse().toString());

      String s = "How  many	 words   here";
      int nw = countWords (s);
      System.out.println (s + "\t" + nw);

      String harder = "HHHHowwwww      mannnnny  words heeere        " +
              " must count          seven  ";
      System.out.println(harder + " => " + countWords(harder));

      // Time measuring

       long startTime = new Date().getTime();

       TimeUnit.SECONDS.sleep(3);

       long endTime = new Date().getTime();

       long timeSlept = endTime - startTime;

       System.out.println("difference between these two moments: " + timeSlept);


      final int LSIZE = 100;
      ArrayList<Integer> randList = new ArrayList<>(LSIZE);
      Random generaator = new Random();
      for (int i=0; i<LSIZE; i++) {
         randList.add (generaator.nextInt(1000));
      }
      System.out.println("randList: " + randList);

       // minimal element

       System.out.println ("Minimum: " + minimum (randList));

      // HashMap tasks:
      //    create
      //    print all keys
      //    remove a key
      //    print all pairs

       Map<String, String> map = new HashMap<>();
       map.put("ICD0007", "Veebitehnoloogiad");
       map.put("ICD0001", "Algoritmid ja andmestruktuurid");
       map.put("ICY0004", "IT eetilised, sotsiaalsed ja professionaalsed aspektid");
       map.put("ITI0101", "Sissejuhatus infotehnoloogiasse");
       map.put("ICY0030", "Kõrgem matemaatika");

       System.out.println(map.keySet());

       map.remove("ICD0007");

       System.out.println(map);

       map.forEach((key, value) -> System.out.println("subject code: " + key + " subject name: " + value));


      System.out.println ("Before reverse:  " + randList);
      reverseList (randList);
      System.out.println ("After reverse: " + randList);

      System.out.println ("Maximum: " + maximum (randList));
   }

    static public <T extends Object & Comparable<? super T>>
    T minimum (Collection<? extends T> a)
            throws NoSuchElementException {
        return Collections.min(a);
    }

   /** Finding the maximal element.
    * @param a Collection of Comparable elements
    * @return maximal element.
    * @throws NoSuchElementException if <code> a </code> is empty.
    */
   static public <T extends Object & Comparable<? super T>>
         T maximum (Collection<? extends T> a) 
            throws NoSuchElementException {
      return Collections.max(a);
   }

   /** Counting the number of words. Any number of any kind of
    * whitespace symbols between words is allowed.
    * @param text text
    * @return number of words in the text
    */
   public static int countWords (String text) {
       if (text == null || text.isEmpty()) {
           return 0;
       }
       StringTokenizer tokens = new StringTokenizer(text);
       return tokens.countTokens();
   }

   /** Case-reverse. Upper -> lower AND lower -> upper.
    * @param s string
    * @return processed string
    */
   public static String reverseCase (String s) {
       StringBuilder newString = new StringBuilder(s);

       for(int i = 0; i < s.length(); i++) {
           if(Character.isLowerCase(s.charAt(i))) {
               newString.setCharAt(i, Character.toUpperCase(s.charAt(i)));
           }
           else if(Character.isUpperCase(s.charAt(i))) {
               newString.setCharAt(i, Character.toLowerCase(s.charAt(i)));
           }
       }

       return newString.toString();
   }

   /** List reverse. Do not create a new list.
    * @param list list to reverse
    */
   public static <T> void reverseList (List<T> list)
      throws UnsupportedOperationException {
         Collections.reverse(list);
   }
}
